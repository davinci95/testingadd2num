function addStrings(str1, str2) {
  let str1a = str1.split("").reverse();
  let str2a = str2.split("").reverse();
  let output = "";
  let longer = Math.max(str1.length, str2.length);
  let carry = false;
  let result;

  for (let i = 0; i < longer; i++) {
    if (str1a[i] && str2a[i]) {
      result = parseInt(str1a[i]) + parseInt(str2a[i]);
      console.log(`${parseInt(str1a[i])} + ${parseInt(str2a[i])} = ${result}`);
    } else if (str1a[i] && !str2a[i]) {
      result = parseInt(str1a[i]);
    } else if (!str1a[i] && str2a[i]) {
      result = parseInt(str2a[i]);
    }

    if (carry) {
      console.log(`${result} + 1 =`);
      result += 1;
      carry = false;
    }
    if (result >= 10) {
      carry = true;
      output += result.toString()[1];

      console.log(
        `${result} memorize ${result.toString()[0]} we have ${output
          .split("")
          .reverse()
          .join("")} `
      );
    } else {
      output += result.toString();
      console.log(
        `${result} down ${result} we have ${output
          .split("")
          .reverse()
          .join("")}`
      );
    }
  }
  output = output.split("").reverse().join("");

  if (carry) {
    output = "1" + output;
    console.log(`down 1 the result is ${output}`);
  }
  return output;
}
addStrings("100", "98");

// test
function testEquals(expected, actual) {
  if (expected == actual) console.log("OK: " + expected);
  else console.error("ERROR: " + expected + " != " + actual);
}
